#include <stdio.h>

int main() {
    float nombre1, nombre2;
    char operation;

    printf("Entrez le premier nombre : ");
    scanf("%f", &nombre1);

    printf("Entrez le deuxième nombre : ");
    scanf("%f", &nombre2);

    printf("Entrez l'opération à réaliser (+, -, *, /) : ");
    scanf(" %c", &operation);

    switch(operation) {
        case '+':
            printf("Le résultat est : %.2f\n", nombre1 + nombre2);
            break;
        case '-':
            printf("Le résultat est : %.2f\n", nombre1 - nombre2);
            break;
        case '*':
            printf("Le résultat est : %.2f\n", nombre1 * nombre2);
            break;
        case '/':
            if(nombre2 != 0)
                printf("Le résultat est : %.2f\n", nombre1 / nombre2);
            else
                printf("Erreur : division par zéro.\n");
            break;
        default:
            printf("Opération invalide.\n");
            break;
    }

    return 0;
}

