#include<stdio.h>
#include<stdlib.h>

// double au lieu de float (plus précis)

double add(double x, double y) {
    return x + y;
}

double sub(double x, double y) {
    return x - y;
}

double mul(double x, double y) {
    return x * y;
}

// divv pour ne pas avoir de conflit avec div 
// défini dans stdlib.h
double divv(double x, double y) {
    return x / y;
}

int main() {
    double a,b,res;
    char op;

    // %f pour un float, %lf pour un double

    printf("Première valeur : ");
    scanf("%lf", &a);
    printf("Seconde valeur  : ");
    scanf("%lf", &b);
    printf("Operation [+-*/] : ");
    scanf(" %c", &op);

    printf("Calcul de : %.2lf %f %c\n", a, b, op);

    switch (op) {
        case '+':
           res = add(a, b);
           break;
        case '-':
           res = sub(a, b);
           break;
        case '*':
           res = mul(a, b);
           break;
        case '/':
           res = divv(a, b);
           break;
        default:
           printf("Opération inconnue.\n");
           exit(EXIT_FAILURE);
    } 
    printf("Résultat : %.2lf\n", res);
    return 0;
}



