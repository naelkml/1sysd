#include <stdio.h>
#include <ctype.h>

void invertcase(char *s) {
    while (*s != '\0') {
        if (isalpha(*s)) {
            *s = (*s == toupper(*s)) ? tolower(*s) : toupper(*s);
        }
        s++;
    }
}

int main(int argc, char *argv[]) {
    if (argc != 2) {
        printf("Usage: ./invertcase <string>\n");
        return 1;
    }

    invertcase(argv[1]);
    printf("%s\n", argv[1]);

    return 0;
}
