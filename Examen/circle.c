#include <stdio.h>

#define PI 3.14159

double P(double r) {
    return 2 * PI * r;
}

double S(double r) {
    return PI * r * r;
}

int main() {
    double rayon;

    printf("Ecrivez la valeur du rayon: ");
    scanf("%lf", &rayon);
    printf("Perimetre du cercle : %.4f\n", P(rayon));
    printf("Surface du disque : %.4f\n", S(rayon));
    return 0;
}

